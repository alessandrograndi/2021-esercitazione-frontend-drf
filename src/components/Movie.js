import {FaTrash} from "react-icons/fa"
import React from "react"
import PropTypes from 'prop-types'
import axios from "axios";

const Movie = (props) => {

    const deleteMovie = async (id) => {
        await axios.delete("http://127.0.0.1:8000/api/movies/" + id + "/").then(
            res => {props.get();}
        ).catch(
            err => {
                console.log(err)
            });
    }

    return (
        <div className="movie">
            <div className="movie-metadata">
                <h3 className="title">{props.movie.title}</h3>
                <p className="subtitle">{props.movie.subtitle}</p>
            </div>
            <div className="movie-action">
                <FaTrash style={{color: 'red', fontSize: '120%'}}
                         onClick={() => {
                            deleteMovie(props.movie.id)
                        }}/>
            </div>
        </div>
    )
}

Movie.propTypes = {
    movie: PropTypes.object,
}

export default Movie
