import React from 'react'
import axios from "axios";
import Movie from "./Movie";
import AddMovie from "./AddMovie";
import {Button} from "react-bootstrap";

class MovieList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [],
            addMovie: false,
            newText: 'Nuovo +'
        }
    }

    componentDidMount() {
        this.getMovies();
    }

    getMovies() {
        axios.get("http://127.0.0.1:8000/api/movies/")
            .then(
                res => {
                    this.setState({movies: res.data});
                }
            )
            .catch(err => {
                console.log(err)
            });
    }

    toggleNew() {
        this.setState({
            addMovie: (!this.state.addMovie),
            newText: this.state.newText === 'Nuovo +' ? 'Annulla' : 'Nuovo +',
        });
    }

    render() {
        return (
            <div className="movies box">
                <h2 className="movie-header">I tuoi film preferiti</h2>
                {/*<hr/>*/}
                <Button variant="info" onClick={this.toggleNew.bind(this)}>
                    {this.state.newText}
                </Button>
                <hr/>
                {
                    this.state.addMovie ?
                        <AddMovie get={this.getMovies.bind(this)} onAdd={this.toggleNew.bind(this)}/> :
                        this.state.movies.map(movie => (
                            <Movie key={movie.id} movie={movie} get={this.getMovies.bind(this)}/>
                        ))
                }
            </div>
        )
    }

}

export default MovieList
